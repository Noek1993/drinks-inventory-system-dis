/*
 * This file was automatically generated by EvoSuite
 * Wed Jun 15 11:00:46 GMT 2016
 */

package org.dis.server.database;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.dis.server.database.UserDatabase;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true) 
public class UserDatabase_ESTest extends UserDatabase_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test0()  throws Throwable  {
      UserDatabase userDatabase0 = new UserDatabase();
      // Undeclared exception!
      try { 
        userDatabase0.getUserById((-52L));
        fail("Expecting exception: ExceptionInInitializerError");
      
      } catch(ExceptionInInitializerError e) {
         //
         // no message in exception (getMessage() returned null)
         //
         assertThrownBy("org.dis.server.database.HibernateUtil", e);
      }
  }

  @Test(timeout = 4000)
  public void test1()  throws Throwable  {
      UserDatabase userDatabase0 = new UserDatabase();
      // Undeclared exception!
      try { 
        userDatabase0.getAllUsers();
        fail("Expecting exception: ExceptionInInitializerError");
      
      } catch(ExceptionInInitializerError e) {
         //
         // no message in exception (getMessage() returned null)
         //
         assertThrownBy("org.dis.server.database.HibernateUtil", e);
      }
  }

  @Test(timeout = 4000)
  public void test2()  throws Throwable  {
      UserDatabase userDatabase0 = new UserDatabase();
      // Undeclared exception!
      try { 
        userDatabase0.createUser("Qm5@-5`! >Lwws", "");
        fail("Expecting exception: ExceptionInInitializerError");
      
      } catch(ExceptionInInitializerError e) {
         //
         // no message in exception (getMessage() returned null)
         //
         assertThrownBy("org.dis.server.database.HibernateUtil", e);
      }
  }
}
