/**
 * 
 */
package org.dis.server.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * @author Peter Wessels
 *
 */

@Entity
public class Consumption {
	@Id
	@GeneratedValue
	private long id;

	@Column(name = "consumedOnDate", columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date consumedOnDate; 
	
	@ManyToOne
	private User user;

	
	/**
	 * 
	 */
	public Consumption() {
		super();
	}

	/**
	 * @param consumedOnDate
	 * @param user
	 */
	public Consumption(final Date consumedOnDate, final User user) {
		super();
		this.consumedOnDate = consumedOnDate;
		this.user = user;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final long id) {
		this.id = id;
	}

	/**
	 * @return the consumedOnDate
	 */
	public Date getConsumedOnDate() {
		return consumedOnDate;
	}

	/**
	 * @param consumedOnDate the consumedOnDate to set
	 */
	public void setConsumedOnDate(final Date consumedOnDate) {
		this.consumedOnDate = consumedOnDate;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(final User user) {
		this.user = user;
	}
	
}
