package org.dis.server.models;

import java.util.List;

public class UserContainer {
	private List<User> users;

	public UserContainer(final List<User> users) {
		this.users = users;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(final List<User> users) {
		this.users = users;
	}

}
