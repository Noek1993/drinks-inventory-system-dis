package org.dis.server.models;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Peter Wessels
 * @author Koen Mulder
 *
 */
@Table
@Entity(name = "User_table")
public class User {
	@Id
	@GeneratedValue
	private long id;

	private String firstname;
	private String lastname;
	private int balance;
	
	@OneToMany(mappedBy="user")
	private Collection<Consumption> consumptions = new ArrayList<>();

	/**
	 * Constructor
	 * 
	 * @param firstname
	 * @param lastname
	 */
	public User(final String firstname, final String lastname) {
		this.firstname = firstname;
		this.lastname = lastname;
	}

	public User() {
	}

	/**
	 * Updates the balance of the user and calls the method that update the inventory.
	 * 
	 * @param consume
	 */
	public void updateBalance(final int price) {
		this.balance += price;
	}

	/**
	 * Getter for the firstname
	 * 
	 * @return
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * Setter for the firstname
	 * 
	 * @return
	 */
	public void setFirstname(final String firstname) {
		this.firstname = firstname;
	}

	/**
	 * Getter for the lastname
	 * 
	 * @return
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * Setter for the lastname
	 * 
	 * @return
	 */
	public void setLastname(final String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return the balance
	 */
	public int getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(final int balance) {
		this.balance = balance;
	}
	
	/**
	 * 
	 * @return
	 */
	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "User: " + firstname + " " + lastname;
	}

}
