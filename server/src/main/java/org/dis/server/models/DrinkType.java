package org.dis.server.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.dis.server.exceptions.NotInStockException;

@Table
@Entity(name = "DrinkType")
public class DrinkType {

	@Id
	@GeneratedValue
	private long id;
	private String typeName;
	private int stock;
	private int price;

	/**
	 * 
	 */
	public DrinkType() {
		super();
	}

	/**
	 * @param typeName
	 * @param stock
	 * @param price
	 */
	public DrinkType(final String typeName, final int stock, final int price) {
		super();
		this.typeName = typeName;
		this.stock = stock;
		this.price = price;
	}

	/**
	 * Decrease
	 * 
	 * @param amount
	 * @throws NotInStockException
	 */
	public void decreaseStock(final int amount) throws NotInStockException {
		int newStock = this.stock - amount;
		if(amount <= 0) {
			throw new IllegalArgumentException();
		}
		if (newStock >= 0) {
			this.stock -= amount;
		} else {
			throw new NotInStockException();
		}
	}

	/**
	 * Increase the stock
	 * 
	 * @param amount
	 * @throws NotInStockException
	 */
	public void increaseStock(final int amount) {
		if (amount <= 0) {
			throw new IllegalArgumentException("amount can not be negative");
		}

		this.stock = this.stock + amount;
	}

	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(final int price) {
		if (price < 0) {
			throw new IllegalArgumentException("The price could not be less then zero.");
		} else {
			this.price = price;
		}
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @return the stock
	 */
	public int getStock() {
		return stock;
	}

	/**
	 * @param stock
	 *            the stock to set
	 */
	public void setStock(final int stock) {
		if(stock < 0) {
			throw new IllegalArgumentException("The stock cannot be negative.");
		}
		this.stock = stock;
	}

	/**
	 * @return the typeName
	 */
	public String getTypeName() {
		return typeName;
	}

	/**
	 * @param typeName
	 *            the typeName to set
	 */
	public void setTypeName(final String typeName) {
		this.typeName = typeName;
	}

}
