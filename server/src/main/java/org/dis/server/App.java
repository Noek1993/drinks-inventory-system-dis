package org.dis.server;

import org.dis.server.database.HibernateUtil;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 * Hello world!
 *
 */
public class App{
	public static void main(final String[] args) throws Exception {
		HibernateUtil.getSessionFactory();

		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/");

		Server jettyServer = new Server(8080);
		jettyServer.setHandler(context);

		ServletHolder jerseyServlet = context.addServlet(
			org.glassfish.jersey.servlet.ServletContainer.class, "/*");
		jerseyServlet.setInitOrder(0);

		// Tells the Jersey Servlet which REST service/class to load.
		jerseyServlet.setInitParameter("jersey.config.server.provider.packages", "org.dis.server.rest");
		// the services reside
		// jerseyServlet.setInitParameter("jersey.api.json.POJOMappingFeature", "true");

		jettyServer.start();
		jettyServer.join();

	}
}
