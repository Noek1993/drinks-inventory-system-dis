package org.dis.server.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.dis.server.database.IUserDatabase;
import org.dis.server.database.UserDatabase;
import org.dis.server.models.User;
import org.dis.server.models.UserContainer;

/**
 * REST API for user management
 * 
 * @author Koen
 *
 */
@Path("/user")
public class UserService {

	private IUserDatabase userData;

	/**
	 * Constructor using a UserDatabase
	 */
	public UserService() {
		userData = new UserDatabase();
	}

	public UserService(IUserDatabase userData) {
		this.userData = userData;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public User getUser(@PathParam("id") final Long userId) {
		if (userId == null || userId < 1) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		// Load from database

		User user;
		try {
			user = userData.getUserById(userId);
		} catch (Exception e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		if (user == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		return user;
	}

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public UserContainer getAllUsers() {
		// Load from database
		List<User> users = userData.getAllUsers();
		return new UserContainer(users);
	}

	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	public User createUser(@QueryParam("firstname") final String firstname, @QueryParam("lastname") final String lastname) {
		if (firstname == null || firstname.trim().isEmpty() || lastname == null || lastname.trim().isEmpty()) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		User user = userData.createUser(firstname, lastname);
		return user;
	}
}
