package org.dis.server.rest;

import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.dis.server.database.IInventoryDatabase;
import org.dis.server.database.InventoryDatabase;
import org.dis.server.exceptions.NotInStockException;
import org.dis.server.models.DrinkType;
import org.hibernate.HibernateException;

@Path("/inventory")
public class InventoryService {

	private IInventoryDatabase invData;

	public InventoryService() {
		super();
		this.invData = new InventoryDatabase();
	}

	public InventoryService(IInventoryDatabase invData) {
		super();
		this.invData = invData;
	}

	/**
	 * Called to register a change in the inventory. Updates also the balance of the consumer
	 * 
	 * @param drinkID
	 *            The id of the type of drink
	 * @param userID
	 *            The id of the user
	 * @return The drink
	 */
	@PUT
	@Path("/drink/{userID}/{drinkID}/")
	@Produces(MediaType.APPLICATION_JSON)
	public DrinkType consumeDrink(@PathParam("drinkID") final Long drinkID, @PathParam("userID") final Long userID) {
		// Check if input values are null
		if (drinkID == null || userID == null) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}

		DrinkType retDrink = null;
		try {
			retDrink = invData.registerConsumption(drinkID, userID);
		} catch (NotInStockException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (HibernateException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}

		return retDrink;
	}

	/**
	 * Adds a type of drink to the database.
	 * 
	 * @param typeName
	 * @param price
	 * @return
	 */
	@POST
	@Path("/drink/")
	@Produces(MediaType.APPLICATION_JSON)
	public DrinkType addType(@QueryParam("typeName") final String typeName, @QueryParam("price") final Integer price) {
		// Check if input values are null
		if (typeName == null || price == null || price <= 0) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}

		DrinkType retDrink = null;
		try {
			retDrink = invData.addDrinkType(typeName, price);
		} catch (HibernateException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}

		return retDrink;
	}

	/**
	 * Adds stock and increases the user's balance
	 * 
	 * @param drinkID
	 * @param userID
	 * @param amount
	 * @return
	 */
	@PUT
	@Path("/stock")
	@Produces(MediaType.APPLICATION_JSON)
	public DrinkType addStock(@QueryParam("drinkID") final Long drinkID, @QueryParam("userID") final Long userID, @QueryParam("amount") final Integer amount) {
		if (drinkID == null || userID == null || amount == null || drinkID < 0 || userID < 0 || amount <= 0) {
			throw new WebApplicationException(Status.BAD_REQUEST);
		}

		DrinkType retDrink = null;
		try {
			retDrink = invData.addInventory(drinkID, userID, amount);
		} catch (HibernateException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}

		return retDrink;
	}

}
