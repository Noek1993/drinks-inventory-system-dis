package org.dis.server.database;

import java.util.List;

import org.dis.server.models.User;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class UserDatabase implements IUserDatabase {

	@Override
	public User getUserById(final long id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {
			// Load from database
			tx = session.beginTransaction();
			User user = (User) session.get(User.class, id);
			session.getTransaction().commit();
			return user;
		} catch (Throwable t) {
			if (tx != null) {
				tx.rollback();
			}
			throw t;
		} finally {
			session.close();
		}
	}

	@Override
	public List<User> getAllUsers() {
		// Load from database
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			List<User> users = (List<User>) session.createCriteria(User.class).list();
			session.getTransaction().commit();
			return users;
		} catch (Throwable t) {
			if (tx != null) {
				tx.rollback();
			}
			throw t;
		} finally {
			session.close();
		}
	}

	@Override
	public User createUser(final String firstname, final String lastname) {
		User user = new User(firstname, lastname);

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {
			// Save to database
			tx = session.beginTransaction();
			session.save(user);
			session.getTransaction().commit();
			return user;
		} catch (Throwable t) {
			if (tx != null) {
				tx.rollback();
			}
			throw t;
		} finally {
			session.close();
		}
	}

}
