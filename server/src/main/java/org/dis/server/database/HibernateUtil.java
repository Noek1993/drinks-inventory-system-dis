package org.dis.server.database;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Class to acces Hibernate
 * 
 * @author Koen
 *
 */
public class HibernateUtil {
	private static SessionFactory sessionFactory;

	private static SessionFactory buildSessionFactory() {
		try {
			// TODO: Using a ServiceRegistry will make the application fail, maybe later try to find out why
			Configuration configuration = new Configuration();
			// ServiceRegistry sr = new
			// ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
			// Create the SessionFactory from hibernate.cfg.xml
			return configuration.configure().buildSessionFactory();
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	/**
	 * Returns the Hibernate session factory, opens a connection if not active
	 * 
	 * @return The Hibernate SessionFactory
	 */
	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			sessionFactory = buildSessionFactory();
		}
		return sessionFactory;
	}

	/**
	 * Shuts down the server
	 */
	public static void shutdown() {
		// Close caches and connection pools
		getSessionFactory().close();
	}
}
