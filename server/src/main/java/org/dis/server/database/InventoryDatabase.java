/**
 * 
 */
package org.dis.server.database;

import java.util.Date;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.dis.server.exceptions.NotInStockException;
import org.dis.server.models.Consumption;
import org.dis.server.models.DrinkType;
import org.dis.server.models.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * @author Peter Wessels
 *
 */
public class InventoryDatabase implements IInventoryDatabase {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.dis.server.database.IInventoryDatabase#registerDrink(long, long)
	 */
	@Override
	public DrinkType registerConsumption(final long drinkID, final long userID) throws NotInStockException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		// Start transaction
		Transaction tx = null;
		User dbUser = null;
		DrinkType dbDrink = null;
		try {
			tx = session.beginTransaction();

			// Get user information and stock information from database
			dbUser = (User) session.get(User.class, userID);
			dbDrink = (DrinkType) session.get(DrinkType.class, drinkID);
			
			// If one or both does not exist, throw exception
			if (dbUser == null || dbDrink == null) {
				throw new WebApplicationException(Status.NOT_FOUND);
			}
			
			Consumption consumption = new Consumption(new Date(), dbUser);

			// Do the real stuff (update stock, update balance)
			dbDrink.decreaseStock(1);

			dbUser.updateBalance(-dbDrink.getPrice());
			
			session.save(consumption);

			// Update both objects
			session.update(dbUser);
			session.update(dbDrink);

			// Commit the updates
			session.getTransaction().commit();
		} catch (NotInStockException e) {
			if (tx != null){				
				tx.rollback();
			}
			throw e;
		} catch (HibernateException e) {
			if (tx != null){				
				tx.rollback();
			}
			throw e;
		} finally {
			session.close();
		}

		return dbDrink;
	}

	@Override
	public DrinkType addDrinkType(final String typeName, final int price) {
		Session session = HibernateUtil.getSessionFactory().openSession();

		// Start transaction
		Transaction tx = null;
		DrinkType newDrink = new DrinkType();
		try {
			tx = session.beginTransaction();

			newDrink.setPrice(price);
			newDrink.setTypeName(typeName);

			session.save(newDrink);

			// Commit the updates
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if (tx != null){				
				tx.rollback();
			}
			throw e;
		} finally {
			session.close();
		}

		return newDrink;
	}

	@Override
	public DrinkType addInventory(final long drinkID, final long userID, final int amount) {
		Session session = HibernateUtil.getSessionFactory().openSession();

		// Start transaction
		Transaction tx = null;
		DrinkType dbDrink = null;
		User dbUser = null;
		try {
			tx = session.beginTransaction();

			// Get user information and stock information from database
			dbUser = (User) session.get(User.class, userID);
			dbDrink = (DrinkType) session.get(DrinkType.class, drinkID);

			// If one or both does not exist, throw exception
			if (dbUser == null || dbDrink == null) {
				throw new NotFoundException("Drink or user not found");
			}

			// Do the real stuff (update stock, update balance)
			dbDrink.increaseStock(amount);
			dbUser.updateBalance(amount * dbDrink.getPrice());

			// Update both objects
			session.update(dbUser);
			session.update(dbDrink);

			// Commit the updates
			session.getTransaction().commit();
		} catch (HibernateException e) {
			if (tx != null){				
				tx.rollback();
			}
			throw e;
		} finally {
			session.close();
		}

		return dbDrink;
	}

}
