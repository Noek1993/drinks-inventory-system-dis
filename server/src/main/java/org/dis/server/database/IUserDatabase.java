package org.dis.server.database;

import java.util.List;

import org.dis.server.models.User;

public interface IUserDatabase {
	User getUserById(long id);

	List<User> getAllUsers();

	User createUser(String firstname, String lastname);
}
