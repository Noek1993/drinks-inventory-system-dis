/**
 * 
 */
package org.dis.server.database;

import org.dis.server.exceptions.NotInStockException;
import org.dis.server.models.DrinkType;

/**
 * @author Peter Wessels
 *
 */
public interface IInventoryDatabase {
	DrinkType registerConsumption(long drinkID, long userID) throws NotInStockException;

	DrinkType addDrinkType(String typeName, int price);

	/**
	 * Adds stock and increases the user's balance
	 * 
	 * @param drinkID
	 * @param userID
	 * @param amount
	 * @return
	 * @throws Exception
	 */
	DrinkType addInventory(long drinkID, long userID, int amount) throws Exception;
}
