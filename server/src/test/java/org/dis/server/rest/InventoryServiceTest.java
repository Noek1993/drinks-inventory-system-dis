/**
 * 
 */
package org.dis.server.rest;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.dis.server.database.IInventoryDatabase;
import org.dis.server.exceptions.NotInStockException;
import org.dis.server.models.DrinkType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Peter Wessels
 *
 */
public class InventoryServiceTest {

	private InventoryService service;
	private InventoryDataMock mock;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		mock = new InventoryDataMock();
		service = new InventoryService(mock);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		mock = null;
		service = null;
	}

	@Test
	public void testConsumeDrinkExceptions() {
		service.addType("test", 100);
		try {
			service.consumeDrink(null, 1L);
			fail("Exception expected");
		} catch (Exception e) {
		}
		try {
			service.consumeDrink(1L, null);
			fail("Exception expected");
		} catch (Exception e) {
		}
		try {
			service.consumeDrink(null, null);
			fail("Exception expected");
		} catch (Exception e) {
		}
		try {
			service.consumeDrink(1L, 5L);
			fail("Exception expected");
		} catch (Exception e) {
		}
	}

	@Test
	public void testConsumeDrink() {
		service.addType("test", 100);
		service.consumeDrink(1L, 1L);
		try {
			service.consumeDrink(1L, 1L);
			fail("Exception expected");
		} catch (WebApplicationException e) {
		}
	}

	@Test
	public void testAddType() {
		try {
			service.addType("test", -1);
			fail("Exception expected");
		} catch (WebApplicationException e) {
		}
		try {
			service.addType(null, 111);
			fail("Exception expected");
		} catch (WebApplicationException e) {
		}
		try {
			service.addType("test", null);
			fail("Exception expected");
		} catch (WebApplicationException e) {
		}
	}
	
	@Test
	public void testAddInventory() {
		service.addType("test", 100);
		try {
			service.addStock(2L, 1L, 1);
			fail("Exception expected");
		} catch (WebApplicationException e) {
		}
		try {
			service.addStock(1L, 2L, 1);
			fail("Exception expected");
		} catch (WebApplicationException e) {
		}
		try {
			service.addStock(null, 1L, 1);
			fail("Exception expected");
		} catch (WebApplicationException e) {
		}
		try {
			service.addStock(1L, null, 1);
			fail("Exception expected");
		} catch (WebApplicationException e) {
		}
		try {
			service.addStock(1L, 1L, null);
			fail("Exception expected");
		} catch (WebApplicationException e) {
		}
		try {
			service.addStock(-1L, 1L, 1);
			fail("Exception expected");
		} catch (WebApplicationException e) {
		}
		try {
			service.addStock(1L, -1L, 1);
			fail("Exception expected");
		} catch (WebApplicationException e) {
		}
		try {
			service.addStock(1L, 1L, -1);
			fail("Exception expected");
		} catch (WebApplicationException e) {
		}
	}

	private static class InventoryDataMock implements IInventoryDatabase {
		private List<DrinkType> drinkTypes = new ArrayList<DrinkType>();

		@Override
		public DrinkType registerConsumption(long drinkID, long userID) throws NotInStockException {
			if (drinkID < 1 || drinkID > drinkTypes.size() || userID != 1L) {
				throw new WebApplicationException(Status.NOT_FOUND);
			}
			DrinkType drink = drinkTypes.get((int) drinkID - 1);
			drink.decreaseStock(1);
			return drink;
		}

		@Override
		public DrinkType addDrinkType(String typeName, int price) {
			DrinkType dt = new DrinkType(typeName, 1, price);
			drinkTypes.add(dt);
			return dt;
		}

		@Override
		public DrinkType addInventory(long drinkID, long userID, int amount) throws Exception {
			if (drinkID < 1 || drinkID > drinkTypes.size() || userID != 1L) {
				throw new WebApplicationException(Status.NOT_FOUND);
			}
			DrinkType drink = drinkTypes.get((int) drinkID - 1);
			drink.increaseStock(amount);
			return drink;
		}
	}

}
