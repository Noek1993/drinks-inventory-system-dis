package org.dis.server.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;

import org.dis.server.database.IUserDatabase;
import org.dis.server.models.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserServiceTest {

	private static final String FIRSTNAME = "Test";
	private static final String LASTNAME = "Person";

	private UserService testService;
	private IUserDatabase userDataMock;

	@Before
	public void setUp() throws Exception {
		userDataMock = new UserDataMock();
		testService = new UserService(userDataMock);
	}

	@After
	public void tearDown() throws Exception {
		userDataMock = null;
		testService = null;
	}

	@Test
	public void testGetUser() {
		// Get non existing user test
		try {
			testService.getUser(10L);
			fail("Exception should be thrown");
		} catch (WebApplicationException t) {
			assertEquals(404, t.getResponse().getStatus());
		}
		// Get test without arguments
		try {
			testService.getUser(null);
			fail("Exception should be thrown");
		} catch (WebApplicationException t) {
			assertEquals(400, t.getResponse().getStatus());
		}
		// Create and get compare test
		User user = testService.createUser(FIRSTNAME, LASTNAME);
		User user2 = testService.getUser(1L);
		assertEquals(user2.getFirstname(), user.getFirstname());
		assertEquals(user2.getLastname(), user.getLastname());
		// Invalid ID Test
		try {
			testService.getUser(0L);
			fail("Exception should be thrown");
		} catch (WebApplicationException t) {
			assertEquals(400, t.getResponse().getStatus());
		}
	}

	@Test
	public void testGetAllUsers() {
		assertEquals(0, testService.getAllUsers().getUsers().size());
		testService.createUser(FIRSTNAME, LASTNAME);
		assertEquals(1, testService.getAllUsers().getUsers().size());
	}

	@Test
	public void testCreateUser() {
		// Good test
		User user = testService.createUser(FIRSTNAME, LASTNAME);
		assertEquals(FIRSTNAME, user.getFirstname());
		assertEquals(LASTNAME, user.getLastname());
		// Lastname null test
		try {
			testService.createUser(FIRSTNAME, null);
			fail("Exception should be thrown");
		} catch (WebApplicationException t) {
			assertEquals(400, t.getResponse().getStatus());
		}
		// Firstname null test
		try {
			testService.createUser(null, LASTNAME);
			fail("Exception should be thrown");
		} catch (WebApplicationException t) {
			assertEquals(400, t.getResponse().getStatus());
		}
		// Both names null test
		try {
			testService.createUser(null, null);
			fail("Exception should be thrown");
		} catch (WebApplicationException t) {
			assertEquals(400, t.getResponse().getStatus());
		}
		// Both names empty test
		try {
			testService.createUser("", "");
			fail("Exception should be thrown");
		} catch (WebApplicationException t) {
			assertEquals(400, t.getResponse().getStatus());
		}
	}

	private static class UserDataMock implements IUserDatabase {
		private List<User> users = new ArrayList<User>();

		@Override
		public User getUserById(long id) {
			if (id < 1) {
				throw new RuntimeException();
			}
			if (id > users.size()) {
				return null;
			}
			return users.get((int) id - 1);
		}

		@Override
		public List<User> getAllUsers() {
			return users;
		}

		@Override
		public User createUser(String firstname, String lastname) {
			User user = new User(firstname, lastname);
			users.add(user);
			return user;
		}
	}
}
