package org.dis.server.models;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserTest {

	private static final String FIRSTNAME = "Test";
	private static final String LASTNAME = "Person";
	private static final String SECOND_FIRSTNAME = "Name";
	private static final String SECOND_LASTNAME = "People";

	private User testUser;

	@Before
	public void setUp() throws Exception {
		testUser = new User(FIRSTNAME, LASTNAME);
	}

	@After
	public void tearDown() throws Exception {
		testUser = null;
	}

	@Test
	public void testGetFirstname() {
		assertEquals(FIRSTNAME, testUser.getFirstname());
	}

	@Test
	public void testSetFirstname() {
		assertEquals(FIRSTNAME, testUser.getFirstname());
		testUser.setFirstname(SECOND_FIRSTNAME);
		assertEquals(SECOND_FIRSTNAME, testUser.getFirstname());
	}

	@Test
	public void testGetLastname() {
		assertEquals(LASTNAME, testUser.getLastname());
	}

	@Test
	public void testSetLastname() {
		assertEquals(LASTNAME, testUser.getLastname());
		testUser.setLastname(SECOND_LASTNAME);
		assertEquals(SECOND_LASTNAME, testUser.getLastname());
	}

	@Test
	public void testInitialBalance() {
		assertEquals(0, testUser.getBalance());
	}

	@Test
	public void testSetBalance() {
		testUser.setBalance(100);
		assertEquals(100, testUser.getBalance());
	}

	@Test
	public void testUpdateBalance() {
		testUser.setBalance(1000);
		testUser.updateBalance(100);
		assertEquals(1100, testUser.getBalance());
		testUser.updateBalance(-1100);
		assertEquals(0, testUser.getBalance());
		testUser.updateBalance(-1000);
		assertEquals(-1000, testUser.getBalance());
		testUser.updateBalance(-1000);
		assertEquals(-2000, testUser.getBalance());
		testUser.updateBalance(0);
		assertEquals(-2000, testUser.getBalance());
	}
}
