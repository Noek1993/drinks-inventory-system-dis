/*
 * This file was automatically generated by EvoSuite
 * Fri Jun 17 11:21:43 GMT 2016
 */

package org.dis.server.models;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.evosuite.runtime.EvoAssertions.*;
import org.dis.server.models.DrinkType;
import org.evosuite.runtime.EvoRunner;
import org.evosuite.runtime.EvoRunnerParameters;
import org.junit.runner.RunWith;

@RunWith(EvoRunner.class) @EvoRunnerParameters(mockJVMNonDeterminism = true, useVFS = true, useVNET = true, resetStaticState = true, separateClassLoader = true) 
public class DrinkType_ESTest extends DrinkType_ESTest_scaffolding {

  @Test(timeout = 4000)
  public void test00()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType();
      drinkType0.setStock(0);
      assertEquals(0, drinkType0.getStock());
  }

  @Test(timeout = 4000)
  public void test01()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType();
      // Undeclared exception!
      try { 
        drinkType0.increaseStock((-43));
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // amount can not be negative
         //
         assertThrownBy("org.dis.server.models.DrinkType", e);
      }
  }

  @Test(timeout = 4000)
  public void test02()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType();
      // Undeclared exception!
      try { 
        drinkType0.decreaseStock(0);
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // no message in exception (getMessage() returned null)
         //
         assertThrownBy("org.dis.server.models.DrinkType", e);
      }
  }

  @Test(timeout = 4000)
  public void test03()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType();
      drinkType0.setTypeName("org.dis.server.exceptions.NotInStockException");
      String string0 = drinkType0.getTypeName();
      assertEquals("org.dis.server.exceptions.NotInStockException", string0);
  }

  @Test(timeout = 4000)
  public void test04()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType("", 1, (-1));
      drinkType0.getTypeName();
      assertEquals(1, drinkType0.getStock());
      assertEquals(-1, drinkType0.getPrice());
  }

  @Test(timeout = 4000)
  public void test05()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType();
      int int0 = drinkType0.getStock();
      assertEquals(0, int0);
  }

  @Test(timeout = 4000)
  public void test06()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType();
      drinkType0.setStock(3615);
      int int0 = drinkType0.getStock();
      assertEquals(3615, int0);
  }

  @Test(timeout = 4000)
  public void test07()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType();
      drinkType0.setPrice(21);
      int int0 = drinkType0.getPrice();
      assertEquals(21, int0);
  }

  @Test(timeout = 4000)
  public void test08()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType((String) null, 1815, (-1133));
      // Undeclared exception!
      try { 
        drinkType0.setStock((-1133));
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // The stock cannot be negative.
         //
         assertThrownBy("org.dis.server.models.DrinkType", e);
      }
  }

  @Test(timeout = 4000)
  public void test09()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType();
      // Undeclared exception!
      try { 
        drinkType0.setPrice((-13));
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // The price could not be less then zero.
         //
         assertThrownBy("org.dis.server.models.DrinkType", e);
      }
  }

  @Test(timeout = 4000)
  public void test10()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType((String) null, 1815, (-1133));
      drinkType0.setPrice(0);
      int int0 = drinkType0.getPrice();
      assertEquals(0, int0);
  }

  @Test(timeout = 4000)
  public void test11()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType();
      // Undeclared exception!
      try { 
        drinkType0.increaseStock(0);
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // amount can not be negative
         //
         assertThrownBy("org.dis.server.models.DrinkType", e);
      }
  }

  @Test(timeout = 4000)
  public void test12()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType();
      drinkType0.increaseStock(1526);
      assertEquals(1526, drinkType0.getStock());
  }

  @Test(timeout = 4000)
  public void test13()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType("Ypv<M$+N!RzhgB", 1411, 1411);
      drinkType0.decreaseStock(1411);
      try { 
        drinkType0.decreaseStock(1411);
        fail("Expecting exception: Exception");
      
      } catch(Exception e) {
         //
         // no message in exception (getMessage() returned null)
         //
         assertThrownBy("org.dis.server.models.DrinkType", e);
      }
  }

  @Test(timeout = 4000)
  public void test14()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType("amount can not be negative", (-1), (-1));
      // Undeclared exception!
      try { 
        drinkType0.decreaseStock((-1071));
        fail("Expecting exception: IllegalArgumentException");
      
      } catch(IllegalArgumentException e) {
         //
         // no message in exception (getMessage() returned null)
         //
         assertThrownBy("org.dis.server.models.DrinkType", e);
      }
  }

  @Test(timeout = 4000)
  public void test15()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType((String) null, 1815, (-1133));
      drinkType0.decreaseStock(1);
      assertEquals(1814, drinkType0.getStock());
  }

  @Test(timeout = 4000)
  public void test16()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType();
      String string0 = drinkType0.getTypeName();
      assertNull(string0);
  }

  @Test(timeout = 4000)
  public void test17()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType("amount can not be negative", (-1), (-1));
      int int0 = drinkType0.getStock();
      assertEquals((-1), int0);
      assertEquals(-1, drinkType0.getPrice());
  }

  @Test(timeout = 4000)
  public void test18()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType();
      long long0 = drinkType0.getId();
      assertEquals(0L, long0);
  }

  @Test(timeout = 4000)
  public void test19()  throws Throwable  {
      DrinkType drinkType0 = new DrinkType((String) null, 1815, (-1133));
      int int0 = drinkType0.getPrice();
      assertEquals((-1133), int0);
      assertEquals(1815, drinkType0.getStock());
  }
}
